Irene Valli - 876361

TTC - Tecnologie e Applicazioni dei Sistemi distribuiti

# Cat Breeds Website

_INDICE_

[TOC]

---


## API utilizzate

Per la creazione del sito è stata utilizzata l'API ["The Cat API"](https://thecatapi.com/).

In particolare gli endpoint utilizzati nell'API sono:

- `/breeds`: per ottenere la lista delle razze
- `/breeds/:id`: per ottenere informazioni e caratteristiche di una singola razza
- `/images/search?breed_id=:id`: per ottenere le immagini di una singola razza

Per accedere alle API è stato necessario richiedere una *key*, presente nel file `cat-api.js` in `src/data`. 

<br>

## Tecnologie utilizzate

Per lo svolgimento del progetto sono stati utilizzati:

- [Create React App](https://create-react-app.dev/)
- [React Router Dom](https://reactrouter.com/) (versione 6)
- [Bootstrap](https://getbootstrap.com/): per lo stile
- [Reactstrap](https://reactstrap.github.io/): per alcuni components come *navbar*, *cards* e tabelle

<br>

## Architettura dell'informazione

L'applicazione è divisa in tre sezioni:

- **Home**: presenta una CTA che riporta alla sezione cat breeds;
- **Cat breeds**: mostra tutte le razze feline con due visualizzazioni, griglia e tabella. Ogni razza ha la propria CTA che porta alla pagina di dettaglio in cui sono presenti varie caratteristiche e informazioni;
- **Random cat**: pagina in cui è presente una CTA che porta alla pagina di dettaglio di una razza scelta in modo random.

<br>

## Struttura delle cartelle del progetto

Nella cartella `src` del progetto sono presenti le seguenti cartelle:

- `assets`: contiene il logo del sito e l'immagine presente nella home
- `components`: contiene tutti i component utilizzati all'interno dell'applicazione:
  - `CatBreeds`:
    - `CatBreedsGrid`: gestisce la visualizzazione a griglia nella sezione Cat Breeds
    - `CatBreedsList`: gestisce la visualizzazione a tabella nella sezione Cat Breeds
  - `CatCard`: card presenti nella visualizzazione a griglia della sezione Cat Breeds
  - `Footer`
  - `Navbar`
- `Data`: contiene un file js con l'url e la key per l'API
- `Views`: contiene le view dell'applicazione
  - `CatBreed`: view per la pagina di dettaglio della razza
  - `CatBreeds`: view per vedere tutte le razze feline, contiene lo switch che permette di passare dalla visualizzazione a griglia a quella a lista
  - `Home`: view della home
  - `RandomCat`: view della sezione random cat

<br>

## Components

### CatBreedsGrid

Il component `CatBreedsGrid` permette di visualizzare tutte le razze feline attraverso delle cards, una per ogni razza.
![CatBreedsGrid Component](readme/CatBreedsGrid.png)

Per creare il component `CatBreedsGrid` è stato utilizzato il component `CardGroup` di Reactstrap. All'interno di `CatBreedsGrid` viene richiamato il component `CatCard`.

<br>

### CatCard

Il component `CatCard` permette di creare una card per ogni singola razza.
La card viene richiamata all'interno del component `CatBreedsGrid`.

Per crearla è stato utilizzato il component `Card` di Reactstrap.

Quando l'immagine del gatto non è presente, viene utilizzato il logo dell'applicazione.

```jsx
<CardImg
  alt={catBreed.name}
  src={catBreed.image?.url || catLogo}
  top
  width="100%"
  className={style.img}
/>
```

<br>

### CatBreedsList

Il component `CatBreedsList` permette di visualizzare le razze feline attraverso una tabella.
![CatBreedsList Component](readme/CatBreedsList.png)

Per creare `CatBreedsList` è stato utilizzato il component `Table` di Reactstrap.

All'interno del file del component `CatBreedsList` è stato creato il component `TableRow` che crea una riga della tabella per ogni singola razza. 

<br>

### Footer

Il component `Footer` si trova in tutte le pagine, viene quindi richiamato dal file `App.js`.

Per poter rendere il footer utilizzabile in diversi contesti, il suo contenuto (`contacts` e `infos`) viene inserito in un oggetto all'interno del file `App.js` e non direttamente nel component.

<br>

### Navbar

Il component `Navbar` si basa sul component `Navbar` di Reactstrap.

Il contenuto della navbar (`brand` e `navItems`) viene passato attraverso _props_ poiché viene inserito in un oggetto nel file `App.js` per poter rendere il component riutilizzabile in diversi contesti.

<br>

## Views

### CatBreed
`CatBreed` è la view dedicata a mostrare i dettagli di ogni singola razza. 
![CatBreed View](readme/CatBreed.png)
![CatBreed View 2](readme/CatBreed2.png)

Per ricavare l'id della razza dalla route viene utilizzato l'hook `useParams()`:
```javascript
let params = useParams();
const id = params.id;
```

Per creare la sezione con le informazioni e le statistiche sono stati creati degli ulteriori component che si trovano direttamente all'interno di `CatBreed`:
- `Circle`: crea i cerchietti delle statistiche (alcune statistiche come *social needs* presentano un valore da 0 a 5 che è stato tradotto in maniera visuale attraverso dei cerchietti colorati)
- `StatisticTitle`: crea i titoli che definiscono le tre sezioni con informazioni e statistiche: informations, life with *{nome della razza}* e needs 
- `StatisticSection`: crea un titolo per ogni statistica e definisce numero di cerchietti da colorare in base alle API 
- `StatisticSectionText`: crea un titolo per ogni informazione che presenta un testo e inserisce l'informazione

<br>

### CatBreeds
`CatBreeds` è la view dedicata a tutte le razze feline. Contiene il bottone che permette di switchare tra la vista a griglia, richiamando il component `CatBreedsGrid`, e la vista a tabella, richiamando il component `CatBreedsList`. 

![CatBreeds View](readme/CatBreeds.png)

Per passare da una visualizzazione all'altra viene definito lo stato `isGrid` tramite l'hook `useState()`. 
```javascript
const [isGrid, setIsGrid] = useState(true)
```

```jsx
isGrid 
    ? <CatBreedsGrid catBreeds={catBreeds} /> 
    : <CatBreedsList catBreeds={catBreeds} />
```

<br>

### Home
La home introduce l'applicazione e presenta una CTA per accedere alla sezione *cat breeds*.
![Home View](readme/home.png)

<br>

### RandomCat
La view `RandomCat` permette di accedere, attraverso una CTA, alla sezione di dettaglio di una razza random. 
![RandomCat View](readme/randomCat.png)

Poiché gli id delle razze nell'API sono abbreviazioni del nome, viene utilizzata la posizione della razza all'interno del JSON restituito dall'API per ottenerne una random. 
```javascript
function randomNumber(max) {
  return Math.floor(Math.random() * max);
}
```

```jsx
<Link to={"/breeds/" + catBreeds?.[randomNumber(catBreeds?.length)].id}>
    <Button color="dark">Radom Cat</Button>
</Link>
```

<br>

## Gestione delle routes
Per la gestione delle routes all'interno dell'applicazione è stato utilizzato React Router Dom nella versione 6.

Il component `<BrowserRouter>` è stato inserito all'interno del file `index.js`. 

Le routes sono state poi generate nel file `App.js` attraverso l'hook `useRoutes()`. In particolare è stato definito l'array `navItems` che contiene gli oggetti che rappresentano le voci della navbar. 

Ogni oggetto ha gli attributi:
- `nome`
- `path`
- `element`: che identifica a quale component si riferisce
- `navbar`: che può avere valori booleani per indicare se quell'oggetto si deve trovare come voce nella navbar

```javascript
const navItems = [
    { 
        name: "home", 
        path: "/", 
        element: <Home />, 
        navbar: "true" },
    {
        name: "cat breeds",
        path: "/breeds",
        element: <CatBreeds />,
        navbar: "true",
    },
    {
        name: "breed",
        path: "/breeds/:id",
        element: <CatBreed />,
        navbar: "false",
    },
    {
        name: "random cat",
        path: "/random-cat",
        element: <RandomCat />,
        navbar: "true",
    },
];

let routes = useRoutes(navItems);
```

La variabile `routes` viene poi richiamata nel `return` di `App`. Che a sua volta si trova all'interno di `<BrowserRouter>` nel file `index.js`.
```jsx
<>
    <Navbar brand={brand} navItems={navItems} />
    {routes}
    <Footer contacts={contacts} infos={infos} />
</>
```
```jsx
<BrowserRouter>
    <App />
</BrowserRouter>
```

## Problemi
### Il problema delle immagini tagliate
Poiché l'API fornisce immagini particolarmente differenti tra loro come formato e orientamento, i gatti non sono sempre ben visibili nelle cards, come dimostra l'immagine sottostante. 
![Immagini di gatti tagliate](readme/problema_immagini.png)

Per risolvere il problema sarebbe necessario integrare nell'applicazione un servizio di *smart crop* tramite API esterne, come ad esempio il servizio a pagamento [imgIX](https://imgix.com/) oppure tramite librerie di terze parti come ad esempio [smartcrop.js](https://github.com/jwagner/smartcrop.js). Quest'ultima, pur essendo gratuita, non è utilizzabile per una così grande quantità di immagini da adattare in real time. Diventerebbe utilizzabile solo in caso di render statico o server-side delle pagine dove tempi più lunghi di generazione sono più tollerabili. 

<br>

### Il problema del blur
Per fare in modo che nella pagina di dettaglio di ogni razza la foto occupi sempre lo stesso spazio, evitando di venire tagliata, è stata utilizzata la proprietà CSS `backdrop-filter: blur(90px)`. Questa proprietà permette di blurrare tutto lo spazio che l'immagine a cui è applicata **non** occupa all'interno del box in cui è contenuta. Questo box, che ha delle dimensioni costanti, contiene la stessa immagine come background con proprietà `background-size: cover`. Il risultato è quello nella foto sottostante. 
![Effetto blur](readme/blur.png)

Questa proprietà CSS non è però ancora supportata da Firefox, come visibile su [Can I use](https://caniuse.com/css-backdrop-filter).
