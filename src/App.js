import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import { useRoutes } from "react-router-dom";
import catLogo from "./assets/img/catLogo.svg";
import Footer from "./components/Footer/Footer";
import CatBreeds from "./views/CatBreeds/CatBreeds";
import CatBreed from "./views/CatBreed/CatBreed";
import RandomCat from "./views/RandomCat/RandomCat";
import Home from "./views/Home/Home";

function App() {
  //navbar info
  const brand = {
    name: "Cats",
    url: "/",
    logo: catLogo,
  };

  const navItems = [
    { name: "home", path: "/", element: <Home />, navbar: "true" },
    {
      name: "cat breeds",
      path: "/breeds",
      element: <CatBreeds />,
      navbar: "true",
    },
    {
      name: "breed",
      path: "/breeds/:id",
      element: <CatBreed />,
      navbar: "false",
    },
    {
      name: "random cat",
      path: "/random-cat",
      element: <RandomCat />,
      navbar: "true",
    },
  ];

  let routes = useRoutes(navItems);

  //footer info
  const contacts = [
    { type: "name: ", contact: "Irene Valli" },
    { type: "matriculation number: ", contact: "876361" },
    {
      type: "mail: ",
      contact: "i.valli1@campus.unimib.it",
      url: "mailto:i.valli1@campus.unimib.it",
    },
  ];

  const infos = [
    { type: "university", information: "Università di Milano-Bicocca" },
    { type: "anno accademico", information: "2021 - 2022" },
    { type: "corso di laurea", information: "TTC" },
  ];

  return (
    <>
      <Navbar brand={brand} navItems={navItems} />

      {routes}

      <Footer contacts={contacts} infos={infos} />
    </>
  );
}

export default App;
