import React from "react";
import { CardGroup } from "reactstrap";
import CatCard from "../../CatCard/CatCard";

function CatBreedsGrid({ catBreeds }) {
  return (
    <div>
      <CardGroup className=" d-flex justify-content-around">
        {catBreeds?.map((catBreed) => (
          <CatCard catBreed={catBreed} key={catBreed.id} />
        ))}
      </CardGroup>
    </div>
  );
}

export default CatBreedsGrid;
