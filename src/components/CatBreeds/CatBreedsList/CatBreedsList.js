import React from "react";
import { Button, Table } from "reactstrap";
import catLogo from "../../../assets/img/catLogo.svg";
import style from "./CardBreedList.module.css";
import { Link } from "react-router-dom";

function CatBreedsList({ catBreeds }) {
  return (
    <div>
      <Table hover>
        <thead>
          <tr>
            <th>Image</th>
            <th>Breed</th>
            <th>Origin</th>
            <th>Mew!</th>
          </tr>
        </thead>
        <tbody>
          {catBreeds?.map((catBreed) => (
            <TableRow key={catBreed.id} catBreed={catBreed} />
          ))}
        </tbody>
      </Table>
    </div>
  );
}

function TableRow({ catBreed }) {
  return (
    <tr
      style={{
        display: "table-row",
        textDecoration: "none",
        color: "currentcolor",
      }}
    >
      <th scope="row">
        <div>
          <img
            alt={catBreed.name}
            src={catBreed.image?.url || catLogo}
            className={style.img}
          />
        </div>
      </th>
      <td>{catBreed.name}</td>
      <td>{catBreed.origin}</td>
      <td>
        <Link to={"/breeds/" + catBreed.id}>
          <Button color="dark">Mew!</Button>
        </Link>
      </td>
    </tr>
  );
}

export default CatBreedsList;
