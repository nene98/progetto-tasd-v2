import React from "react";
import {
  Card as BSCard,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  Button,
} from "reactstrap";
import style from "./CatCard.module.css";
import catLogo from "../../assets/img/catLogo.svg";
import { Link } from "react-router-dom";

const CatCard = ({ catBreed }) => {
  return (
    <div>
      <BSCard outline color="grey" className={style.card}>
        <div className={style.imgContainer}>
          <CardImg
            alt={catBreed.name}
            src={catBreed.image?.url || catLogo}
            top
            width="100%"
            className={style.img}
          />
        </div>
        <CardBody className="d-flex flex-column align-items-center">
          <CardTitle tag="h5" className="text-center">
            {catBreed.name}
          </CardTitle>
          <CardText>{catBreed.origin}</CardText>
          <Link to={"/breeds/" + catBreed.id}>
            <Button color="dark">Mew!</Button>
          </Link>
        </CardBody>
      </BSCard>
    </div>
  );
};

export default CatCard;
