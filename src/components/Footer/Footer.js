import React from "react";
import style from "./Footer.module.css";

function Footer({ contacts, infos }) {
  return (
    <>
      <div className={style.container}>
        <div className="text-dark">
          <ul className="list-unstyled text-center">
            {contacts.map((contact) => (
              <li key={contact.type}>{contact.contact}</li>
            ))}
          </ul>
        </div>

        <div className={style.logo}></div>

        <div>
          <ul className="list-unstyled text-center">
            {infos.map((info) => (
              <li key={info.type}>{info.information}</li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
}

export default Footer;
