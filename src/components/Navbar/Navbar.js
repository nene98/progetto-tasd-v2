import React from "react";
import {
  Navbar as BSNavbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";
import { NavLink as RRNavLink } from "react-router-dom";
import style from "./Navbar.module.css";

function Navbar({ brand, navItems, navText }) {
  const createNavItem = (navItem) => {
    return (
      <NavItem key={navItem.path}>
        <NavLink tag={RRNavLink} to={navItem.path}>
          {navItem.name}
        </NavLink>
      </NavItem>
    );
  };

  return (
    <div>
      <BSNavbar light expand style={{ backgroundColor: "mistyrose" }}>
        <NavbarBrand href={brand.url}>
          <img src={brand.logo} className={style.logoSize} />
        </NavbarBrand>

        <NavbarToggler onClick={function noRefCheck() {}} />

        <Collapse navbar className="justify-content-end">
          <Nav navbar>
            {navItems
              .filter((navItem) => navItem.navbar == "true")
              .map((navItem) => createNavItem(navItem))}
          </Nav>
          <NavbarText>{navText}</NavbarText>
        </Collapse>
      </BSNavbar>
    </div>
  );
}

export default Navbar;
