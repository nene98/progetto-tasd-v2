import React from "react";
import { useParams } from "react-router-dom";
import style from "./CatBreed.module.css";
import { useEffect, useState } from "react";
import { Col, Container, Row } from "reactstrap";
import { url, key } from "../../data/cat-api";

function CatBreed() {
  let params = useParams();
  const id = params.id;

  const [catBreed, setCatBreed] = useState(null);
  const [catImage, setCatImage] = useState(null);

  //fetch API breed
  useEffect(() => {
    fetch(url + "breeds/" + id + "?key=" + key)
      .then((res) => res.json())
      .catch((err) => console.error(err))
      .then((res) => setCatBreed(res));
  }, []);

  //fetch API images
  useEffect(() => {
    fetch(url + "images/search?breed_id=" + id + "&&limit=5" + "&&key=" + key)
      .then((res) => res.json())
      .catch((err) => console.error(err))
      .then((res) => setCatImage(res));
  }, []);

  return (
    <>
      <div className={style.container}>
        <div
          className={style.imgContainer}
          style={{ backgroundImage: 'url("' + catImage?.[0].url + '")' }}
        >
          <img
            src={catImage?.[0].url}
            alt={catBreed?.name}
            className={style.img}
          />
        </div>

        <div>
          <h1 className="text-center">{catBreed?.name}</h1>
          <p className="h4 text-muted text-center">{catBreed?.origin}</p>
          <p
            className="text-center"
            style={{ width: "50%", marginRight: "auto", marginLeft: "auto" }}
          >
            {catBreed?.description}
          </p>
        </div>
        <br />
        <br />

        <Container>
          <Row>
            <Col>
              <StatisticTitle title="Informations" />
              <StatisticSectionText
                title="Weight"
                apiReferece={catBreed?.weight.metric}
                measure=" kg"
              />
              <StatisticSectionText
                title="Life span"
                apiReferece={catBreed?.life_span}
                measure=" years"
              />
              <StatisticSection
                title="Intelligence"
                apiReferece={catBreed?.intelligence}
              />
              <StatisticSectionText
                title="Temperament"
                apiReferece={catBreed?.temperament}
              />
            </Col>
            <Col>
              <StatisticTitle title={"Life with " + catBreed?.name} />
              <StatisticSection
                title="Social Needs"
                apiReferece={catBreed?.social_needs}
              />
              <StatisticSection
                title="Affection Level"
                apiReferece={catBreed?.affection_level}
              />
              <StatisticSection
                title="Child Friendly"
                apiReferece={catBreed?.child_friendly}
              />
              <StatisticSection
                title="Dog Friendly"
                apiReferece={catBreed?.dog_friendly}
              />
            </Col>
            <Col>
              <StatisticTitle title="Needs" />
              <StatisticSection
                title="Adaptability"
                apiReferece={catBreed?.adaptability}
              />
              <StatisticSection
                title="Energy Level"
                apiReferece={catBreed?.energy_level}
              />
              <StatisticSection
                title="Grooming"
                apiReferece={catBreed?.grooming}
              />
              <StatisticSection
                title="Shedding level"
                apiReferece={catBreed?.shedding_level}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

function StatisticTitle({ title }) {
  return (
    <p className="h5 text-center" style={{ marginBottom: "20px" }}>
      {title}
    </p>
  );
}

function StatisticSection({ title, apiReferece }) {
  return (
    <>
      <p className="h6 text-muted text-center" style={{ marginBottom: "8px" }}>
        {title}
      </p>
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ marginBottom: "25px", height: "24px" }}
      >
        {[0, 1, 2, 3, 4].map((rating) => (
          <Circle
            className={
              apiReferece > rating ? style.bgLightcoral : style.bgMistyrose
            }
            key={rating}
          />
        ))}
      </div>
    </>
  );
}

function StatisticSectionText({ title, apiReferece, measure }) {
  return (
    <>
      <p className="h6 text-muted text-center">{title}</p>
      <p className="text-center" style={{ marginBottom: "25px" }}>
        {apiReferece}
        {measure}
      </p>
    </>
  );
}

function Circle({ className }) {
  return (
    <div
      style={{
        height: "12px",
        width: "12px",
        borderRadius: "100px",
        margin: "2px",
      }}
      className={className}
    ></div>
  );
}

export default CatBreed;
