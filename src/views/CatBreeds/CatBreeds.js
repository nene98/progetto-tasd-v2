import React from "react";
import { useEffect, useState } from "react";
import CatBreedsGrid from "../../components/CatBreeds/CatBreedsGrid/CatBreedsGrid";
import CatBreedsList from "../../components/CatBreeds/CatBreedsList/CatBreedsList";
import { ButtonGroup, Button } from "reactstrap";
import style from "./CatBreeds.module.css";
import { url, key } from "../../data/cat-api";

function CatBreeds() {
  const [catBreeds, setCatBreeds] = useState(null);
  const [isGrid, setIsGrid] = useState(true);

  //fetch API
  useEffect(() => {
    fetch(url + "breeds" + "?key=" + key)
      .then((res) => res.json())
      .catch((err) => console.error(err))
      .then((res) => setCatBreeds(res));
  }, []);

  return (
    <div className={style.container}>
      <div className={style.buttonsContainer}>
        <ButtonGroup className={style.buttonGroup}>
          <Button
            outline
            color="dark"
            active={isGrid}
            onClick={() => setIsGrid(true)}
            className={style.button}
          >
            Grid
          </Button>
          <Button
            outline
            color="dark"
            active={!isGrid}
            onClick={() => setIsGrid(false)}
            className={style.button}
          >
            List
          </Button>
        </ButtonGroup>
      </div>

      {isGrid ? (
        <CatBreedsGrid catBreeds={catBreeds} />
      ) : (
        <CatBreedsList catBreeds={catBreeds} />
      )}
    </div>
  );
}

export default CatBreeds;
