import React from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import style from "./Home.module.css";

function Home() {
  return (
    <div
      id={style.landingPage}
      className="d-flex flex-column align-items-center flex-nowrap"
    >
      <h1>Cat Breeds Website</h1>
      <Link to="/breeds/">
        <Button color="dark">Cat Breeds</Button>
      </Link>
    </div>
  );
}

export default Home;
