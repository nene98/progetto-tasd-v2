import React from "react";
import { Button } from "reactstrap";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { url, key } from "../../data/cat-api";
import catLogo from "../../assets/img/catLogo.svg";
import style from "./RandomCat.module.css";

function randomNumber(max) {
  return Math.floor(Math.random() * max);
}

function RandomCat() {
  const [catBreeds, setCatBreeds] = useState(null);

  //fetch API
  useEffect(() => {
    fetch(url + "breeds" + "?key=" + key)
      .then((res) => res.json())
      .catch((err) => console.error(err))
      .then((res) => setCatBreeds(res));
  }, []);

  return (
    <div
      className="d-flex flex-column justify-content-center align-items-center flex-nowrap"
      style={{ padding: "20px" }}
    >
      <img alt="cat logo" src={catLogo} className={style.logo}></img>
      <h1 className="text-center">Get a random cat breed...'cause, why not?</h1>
      <Link to={"/breeds/" + catBreeds?.[randomNumber(catBreeds?.length)].id}>
        <Button color="dark">Radom Cat</Button>
      </Link>
    </div>
  );
}

export default RandomCat;
